<?php

namespace App\Http\Controllers;

use App\Models\todo_list;
use Illuminate\Http\Request;

class TodoListController extends Controller
{
   // @return \Illuminate\Http\Response
     
    public function index(){
        return view('view_list')->with('todo_arr',todo_list::all());
    }

   // @return \Illuminate\Http\Response
  

    public function create(){
        return view('create_new_list');
    }

    public function store(Request $request){
  //    return  $request->input('name');
      $todo_list = new todo_list();
      $todo_list->name = $request->input('name');
      $todo_list->save();
      return redirect('/');
    }

    public function show(todo_list $todo_list){
        
//
    }

    //public function edit($id){
          
      //   return view('edit_todo')->with('TodoArr_name',todolist::find($id));
    //}

    //public function edit_submit(Request $req, $id){

       
      //   $todo = todolist::find($id);
        // $todo->name = $req->input('name');
         //$todo->save();
         //return redirect('/');
    //}

    public function edit(todo_list $todo_list, $id){
        $todo = todo_list::find($id);
        return view('edit_list')->with('todo_arr',$todo);
        
        
            }

    public function update(Request $request, todo_list $todo_list, $id){
        $todo = todo_list::find($id);
        $todo_list->name = $request->input('name');
        $todo_list->save();
        return redirect('/');
        
            }

    public function destroy(todo_list $todo_list, $id){
        $row = todo_list::destroy($id);
       // $row->destroy();
        return redirect('/');
        
        
                //
                    }

}




